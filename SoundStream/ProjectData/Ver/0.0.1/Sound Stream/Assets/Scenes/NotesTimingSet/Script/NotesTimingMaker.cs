﻿using UnityEngine;
using System.Collections;

public class NotesTimingMaker : MonoBehaviour
{

    public AudioSource _audioSource;
    private float _startTime = 0;
    private int TapNum = 0;
    private CSVWriter _CSVWriter;

    private bool _isPlaying = false;
    public GameObject startButton;

    void Start()
    {
        _CSVWriter = GameObject.Find("CSVWriter").GetComponent<CSVWriter>();
    }

    void Update()
    {
        if (_isPlaying)
        {
            DetectKeys();
        }
    }

    public void StartMusic()
    {
        startButton.SetActive(false);
        _audioSource.Play();
        _startTime = Time.time;
        _isPlaying = true;
    }

    void DetectKeys()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            WriteNotesTiming(TapNum,0,0);
            TapNum++;
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            WriteNotesTiming(TapNum, 1,0);
            TapNum++;
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            WriteNotesTiming(TapNum, 2,0);
            TapNum++;
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            WriteNotesTiming(TapNum, 3,0);
            TapNum++;
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            WriteNotesTiming(TapNum, 4,0);
            TapNum++;
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            WriteNotesTiming(TapNum, 5,0);
            TapNum++;
        }
    }

    void WriteNotesTiming(int num ,int line,int type)
    {
        Debug.Log(GetTiming());
        _CSVWriter.WriteCSV(num.ToString() + "," + GetTiming().ToString() + "," + line.ToString() + "," + type.ToString());
    }

    float GetTiming()
    {
        return Time.time - _startTime;
    }
}
