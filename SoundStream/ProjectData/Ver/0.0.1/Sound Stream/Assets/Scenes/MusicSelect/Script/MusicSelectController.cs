﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Video;


public class MusicSelectController : MonoBehaviour {

    //セレクト画面用曲
    public AudioSource[] Music;
    public VideoPlayer[] Video;
    public static int SelectMusicNum;

    public Text SelectMusicname;
    public Text SelectSinger;
    public Text SelectHiScore;
    public Text SelectEasyLevel;
    public Text SelectNormalLevel;
    public Text SelectHardLevel;

    public AudioSource[] SE;

    public GameObject FadeStart;
    private float Alpha = 1.0f;

    public Animation[] UIanimation;
    public Animation[] SelectUIanimation;

  



    private string MusicNameData;
    private string SingerNameData;
    private string MusicTypeData;
    private string HiScoreData;
    private string EasyLevelData;
    private string NormalLevelData;
    private string HardLevelData;

    // Use this for initialization
    void Start () {

        //起動時に選択されている曲を再生
        Music[SelectMusicNum].Play();
        Video[SelectMusicNum].Play();
        LoadMasterData(SelectMusicNum);
        SelectMusicname.text = MusicNameData;
        SelectSinger.text = SingerNameData;
        SelectHiScore.text = HiScoreData;
        SelectEasyLevel.text = EasyLevelData;
        SelectNormalLevel.text = NormalLevelData;
        SelectHardLevel.text = HardLevelData;

    }
	
	// Update is called once per frame
	void Update () {
        fadestart();
   

    }

    void LoadMasterData(int Num)
    {
        
        TextAsset csv = Resources.Load("CSV/MusicMasterData") as TextAsset;
        //Debug.Log(csv.text);
        StringReader reader = new StringReader(csv.text);
        int i = 0,j;
        while (reader.Peek() > -1)
        {
            string line = reader.ReadLine();
            string[] values = line.Split(',');
            for (j = 0; j < values.Length; j++)
            {
                if (i == Num)
                {
                    MusicNameData = values[0];
                    SingerNameData = values[1];
                    //MusicTypeData = values[2];
                    HiScoreData = values[3];
                    EasyLevelData = values[4];
                    NormalLevelData = values[5];
                    HardLevelData = values[6];
                }
            }
            i++;
        }

    }

    public void MusicChange(int Num)
    {
        SE[0].Play();
        if (SelectMusicNum != Num)
        {
            for (short i = 0; i < Music.Length; i++)
            {
                Music[i].Stop();
                
            }
            for(short i = 0; i < Video.Length; i++)
            {
                Video[i].Stop();
            }

            //UIアニメーション
            for(short i = 0; i < UIanimation.Length; i++)
            {
                if (UIanimation[i]["UIAnimation"].time >= 0)
                {
                    UIanimation[i]["UIAnimation"].speed = -1.0f;
                    UIanimation[i].Play();
                }
            }

            SelectMusicNum = Num;
            Music[Num].Play();
            Video[Num].Play();
            UIanimation[Num]["UIAnimation"].speed = 1.0f;            
            UIanimation[Num].Play();
            LoadMasterData(Num);
            SelectMusicname.text = MusicNameData;
            SelectSinger.text = SingerNameData;
            SelectHiScore.text = HiScoreData;
            SelectEasyLevel.text = EasyLevelData;
            SelectNormalLevel.text = NormalLevelData;
            SelectHardLevel.text = HardLevelData;
            SelectUIanimation[0].Play();
            SelectUIanimation[1].Play();
            SelectUIanimation[2].Play();
            SelectUIanimation[3].Play();
        }
    }

    void fadestart()
    {
        FadeStart.GetComponent<Image>().color = new Color(0f, 0f, 0f, Alpha);
        if (Alpha > 0)
        {
            Alpha -= 0.02f;
        }
        else
        {
            Alpha = 0;
            FadeStart.SetActive(false);
        }
    }




}
