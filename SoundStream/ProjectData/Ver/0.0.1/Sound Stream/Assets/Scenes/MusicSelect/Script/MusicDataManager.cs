﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System;
using UnityEngine.UI;

public class MusicDataManager : MonoBehaviour {

    //マスターデータ参照
    public string filePass;
    //private string filePass = "Resources/CSV/" + fileName;
    

    public int MusicNumber;

    public Text MusicName;
    private string MusicNameData;
    public Text SingerName;
    private string SingerNameData;
    public Text MusicType;
    private string MusicTypeData;
    public Text Hi_Score;
    private string HiScoreData;

    Encoding encoding;

    // Use this for initialization
    void Start()
    {
       
    }

    void Loadmaster()
    {
        int i = 0, j;
        TextAsset csv = Resources.Load("CSV/" + filePass) as TextAsset;
        StringReader reader = new StringReader(csv.text);
        while (reader.Peek() > -1)
        {
            string line = reader.ReadLine();
            string[] values = line.Split(',');
            for (j = 0; j < values.Length; j++)
            {
                if (i == MusicNumber)
                {
                    MusicNameData = values[0];
                    SingerNameData = values[1];
                    MusicTypeData = values[2];
                    HiScoreData = values[3];
                }
                
            }
            i++;

        }
       
    }

    // Update is called once per frame
    void Update () {
        Loadmaster();
        
        MusicName.text = MusicNameData;
        SingerName.text = SingerNameData;
        MusicType.text = MusicTypeData;
        Hi_Score.text = HiScoreData+"P";

    }
}
