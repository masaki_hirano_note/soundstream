﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public GameObject[] notes;
    private float[] _timing;
    private int[] _lineNum;

    public string filePass;
    private int _notesCount = 0;

    public AudioSource _audioSource;
    private float _startTime = 0;

    public float timeOffset = -1;

    private bool _isPlaying = false;
    public GameObject startButton;

    public Text scoreText;
    private int _score = 0;

    public Text comboText;
    static public int _combo = 0;

    public Text maxcomboText;
    private int _maxcombo = 0;

    public Text perfectcountText;
    private int _perfectcount = 0;

    public Text greatcountText;
    private int _greatcount = 0;


    public ParticleSystem tapEffect;
    public ParticleSystem tapEffect2;

    public AudioSource TapSE;

    void Start()
    {
        _timing = new float[1024];
        _lineNum = new int[1024];
        LoadCSV();
        //_audioSource = GetComponent<AudioSource>();
        //TapSE = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (_isPlaying)
        {
            CheckNextNotes();
            scoreText.text = _score.ToString();
            comboText.text ="combo   "+ _combo.ToString();
            maxcomboText.text = "maxcombo" + _maxcombo.ToString();
            perfectcountText.text = "perfect " + _perfectcount.ToString();
            greatcountText.text = "great   " + _greatcount.ToString();
        }
      

    }

    public void StartGame()
    {

            startButton.SetActive(false);
            _startTime = Time.time;
            _audioSource.Play();
            _isPlaying = true;


    }

    void CheckNextNotes()
    {
        while (_timing[_notesCount] + timeOffset < GetMusicTime() && _timing[_notesCount] != 0)
        {
            SpawnNotes(_lineNum[_notesCount]);
            _notesCount++;
        }
    }

    void SpawnNotes(int num)
    {
        Instantiate(notes[num],
            new Vector3(-7.0f + (2.8f * num), 28.0f, 0),
            Quaternion.identity);
    }

    void LoadCSV()
    {
        int i = 0, j;
        TextAsset csv = Resources.Load(filePass) as TextAsset;
        StringReader reader = new StringReader(csv.text);
        while (reader.Peek() > -1)
        {

            string line = reader.ReadLine();
            string[] values = line.Split(',');
            for (j = 0; j < values.Length; j++)
            {
                _timing[i] = float.Parse(values[1]);
                _lineNum[i] = int.Parse(values[2]);
            }
            i++;
        }
    }

    float GetMusicTime()
    {
        return Time.time - _startTime;
    }

    public void PerfectTimingFunc(int num)
    {
        TapSE.Play();
        Instantiate(tapEffect,
            new Vector3(-7.0f + (2.8f * num), 0.0f, 0),
            new Quaternion(180, 0, 0,0));
        Instantiate(tapEffect2,
            new Vector3(-7.0f + (2.8f * num), 0.0f, 0),
            Quaternion.identity);
        Debug.Log("Line:" + num + " Perfect!");
        Debug.Log(GetMusicTime());
        // 追加
       // EffectManager.Instance.PlayEffect(num);
        _score++;
        _perfectcount++;
        _combo++;

        if (_combo >= _maxcombo)
        {
            _maxcombo = _combo;
        }
    }

    public void GreatTimingFunc(int num)
    {
        TapSE.Play();
        Instantiate(tapEffect,
            new Vector3(-7.0f + (2.8f * num), 0.0f, 0),
            new Quaternion(180,0,0,0));
        Instantiate(tapEffect2,
            new Vector3(-7.0f + (2.8f * num), 0.0f, 0),
            Quaternion.identity);
        Debug.Log("Line:" + num + " Great!");
        Debug.Log(GetMusicTime());
        // 追加
        // EffectManager.Instance.PlayEffect(num);
        _score++;
        _greatcount++;
        _combo++;

    if(_combo >= _maxcombo)
        {
            _maxcombo = _combo;
        }
    }

}
