﻿using System.Collections;
using UnityEngine;

public class Notes : MonoBehaviour
{

    public int lineNum;
    private GameController _gameController;
    private bool isInLineP = false;
    private bool isInLineG = false;
    private KeyCode _lineKey;

    void Start()
    {
        _gameController = GameObject.Find("GameController").GetComponent<GameController>();
        _lineKey = GameUtil.GetKeyCodeByLineNum(lineNum);
    }

    void Update()
    {
        this.transform.position += Vector3.down * 30 * Time.deltaTime;

        if (this.transform.position.y < -5.0f)
        {
            Debug.Log("false");
            Destroy(this.gameObject);
            GameController._combo = 0;
        }

        if (isInLineP||isInLineG)
        {
            CheckInput(_lineKey);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (isInLineG)
        {
            isInLineP = true;
        }
        else
        {
            isInLineG = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (isInLineG)
        {
            isInLineP = true;
        }
        else
        {
            isInLineG = true;
        }
    }

    void CheckInput(KeyCode key)
    {

        if (Input.GetKeyDown(key))
        {
            if (isInLineP)
            {
                
                _gameController.PerfectTimingFunc(lineNum);
                Destroy(this.gameObject);
            }
            else if (isInLineG)
            {
                
                _gameController.GreatTimingFunc(lineNum);
                Destroy(this.gameObject);
            }
        }
    }
}