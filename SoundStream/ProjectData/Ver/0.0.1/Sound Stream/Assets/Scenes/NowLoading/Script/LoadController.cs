﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadController : MonoBehaviour
{

    private AsyncOperation async;

    public Slider Slider;

    public static int SceneChangeNum;


    void Start()
    {
        LoadNextScene();

    }

    void Update()
    {

    }

    void LoadNextScene()
    {
       
        StartCoroutine("LoadScene");
    }

    IEnumerator LoadScene()
    {
        if (SceneChangeNum == 0)
        {
            async = SceneManager.LoadSceneAsync("MusicSelect");

            while (!async.isDone)
            {
                Slider.value = async.progress;
                yield return null;
            }
        }
        else if (SceneChangeNum == 1)
        {
            async = SceneManager.LoadSceneAsync("MainMenu");

            while (!async.isDone)
            {
                Slider.value = async.progress;
                yield return null;
            }
        }
    }
}
