﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleController : MonoBehaviour
{
    //シーン管理
    public int SceneNumber;

    //フェード
    public GameObject[] Fade;
    private float Fadealpha =  1;
    private float Fadealpha1 = 1;
    private float Fadealpha2 = 1;
    private float Fadealpha3 = 0;

    //音素材
    public AudioSource BGM;
    private bool BGMPlay = true;
    public AudioSource TapSE;
    private float BGMVolume = 1;

    //タイトル画面挙動
    public GameObject BGScrollEffect;
    private float Alfa = 0.1f;
    public Animation[] LightAnm;
    private short AnmPlayNum = 0;
    private short AnmStartCount = 0;
    public GameObject[] LogoParts;
    private float AnimationCount;
    private bool AnimationFlg = true;
    public GameObject TapStart;
    public Animation TapUI;
    private float TapAlpha = 0.9f;
    private float TapAlphaD = -0.01f;

    //操作
    //タップエフェクト
    // 再生したいEffectのプレハブを指定する
    public GameObject Effect01 = null;




    // Use this for initialization
    void Start()
    {
        Fade[3].SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //タップエフェクト
        TapEffects();


        //ゲーム起動時のUnityロゴ終了フェード
        if (SceneNumber == 0)
        {
            Fade[0].SetActive(true);
            StartUnityFade();
        }
        //Extendロゴフェード
        //3.5秒後に呼び出す  
        if (SceneNumber == 1)
        {
            Fade[1].SetActive(true);
            Invoke("StartExtendFade", 2f);
        }
        //背景フェード
        if (SceneNumber == 2)
        {
            StartBGFade();
            
        }
        //背景エフェクト
        if (SceneNumber == 2 || SceneNumber == 3)
        {
            BGScrollEffects();
            BGLightAnim();
            TapToStartMove();
            if (AnimationFlg == true)
            {
                LogoPartsLightEffect();
            }
            else if(AnimationFlg == false)
            {
                Invoke("LogoPartsLightEffectStart", 3f);
            }
        }

        //操作
        if(SceneNumber == 3)
        {
            if (Input.GetMouseButtonUp(0))
            {
                TapSE.Play();
                TapUI.Play();
                SceneNumber = 4;
            }
        }
        if(SceneNumber == 4)
        {
            BGScrollEffects();
            BGLightAnim();
            BGMVolumeChange();
            if(BGMVolume == 0)
            {
                SceneChangeFade();
            }
        }

    }

    void StartUnityFade()
    {

        Fade[0].GetComponent<Image>().color = new Color(1f, 1f, 1f, Fadealpha);
        if (Fadealpha >= 0)
        {
            Fadealpha -= 0.02f;
        }
        else
        {
            Fadealpha = 0;
            SceneNumber = 1;
            Fade[0].SetActive(false);
        }

        if (Input.GetMouseButtonDown(0))
        {
            Fadealpha = 0;
            SceneNumber = 1;
            Fade[0].SetActive(false);       
        }

    }

    void StartExtendFade()
    {
        
        Fade[1].GetComponent<Image>().color = new Color(1f, 1f, 1f, Fadealpha1);
        if (Fadealpha1 >= 0)
        {
            Fadealpha1 -= 0.02f;
        }
        else
        {
            Fadealpha1 = 0;
            SceneNumber = 2;
            if (BGMPlay)
            {
                BGM.Play();
                BGMPlay = false;
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            Fadealpha1 = 0;
            SceneNumber = 2;
            Fade[1].SetActive(false);
            if (BGMPlay)
            {
                BGM.Play();
                BGMPlay = false;
            }
        }
    }

    void StartBGFade()
    {

        Fade[2].GetComponent<Image>().color = new Color(1f, 1f, 1f, Fadealpha2);
        if (Fadealpha2 >= 0)
        {
            Fadealpha2 -= 0.02f;
        }
        else
        {
            Fadealpha2 = 0;
            SceneNumber = 3;
            
        }
        if (Input.GetMouseButtonDown(0))
        {
            Fadealpha1 = 0;
            SceneNumber = 3;
            Fade[2].SetActive(false);
            
        }
    }

    void BGScrollEffects()
    {
        //背景エフェクトスクロール
        BGScrollEffect.transform.Translate(-0.02f, 0, 0);
        if (BGScrollEffect.transform.position.x < -8.88f)
        {
            BGScrollEffect.transform.position = new Vector3(9.6f, 0.4f, 0);
        }

    }

    void BGLightAnim()
    {
        AnmStartCount++;

        if(AnmStartCount == 15)
        {
            LightAnm[AnmPlayNum].Play();
            if (AnmPlayNum < LightAnm.Length-1)
            {
                AnmPlayNum++;
            }
            else
            {
                AnmPlayNum = 0;
            }
            AnmStartCount = 0;
        }
    }

    void LogoPartsLightEffect()
    {
        for(short i = 0; i < LogoParts.Length; i++)
        {
            LogoParts[i].SetActive(false);
        }
        LogoParts[(int)AnimationCount / 5].SetActive(true);
        AnimationCount++;
        if (AnimationCount == 11*5)
        {
            AnimationCount = 0;
            for (short i = 0; i < LogoParts.Length; i++)
            {
                LogoParts[i].SetActive(false);
            }
            AnimationFlg = false;
        }
    }

    void LogoPartsLightEffectStart()
    {
        AnimationFlg = true;
    }

    void TapToStartMove()
    {
        //フェード
       TapStart.GetComponent<Image>().color = new Color(1f, 1f, 1f, TapAlpha);
        if (TapAlpha >= 1 || TapAlpha <= 0.2f)
        {
            TapAlphaD *= -1f;
        }
        TapAlpha += TapAlphaD;
    }

    void BGMVolumeChange()
    {
        BGM.volume = BGMVolume;
        if (BGMVolume >= 0)
        {
            BGMVolume -= 0.02f;
        }
        else
        {
            BGMVolume = 0;
        }
    }

    void SceneChangeFade()
    {
        Fade[3].SetActive(true);
        Fade[3].GetComponent<Image>().color = new Color(1f, 1f, 1f, Fadealpha3);
        if (Fadealpha3 <= 1)
        {
            Fadealpha3 += 0.02f;
        }
        else
        {
            Fadealpha3 = 1;
            LoadController.SceneChangeNum = 0;
            SceneManager.LoadSceneAsync("NowLoading");
        }
    }

    void TapEffects()
    {
        // タッチエフェクトを仮実装
        if (Input.GetMouseButton(0))
        {
            // タッチした画面座標からワールド座標へ変換
            Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 5.0f));
            // 指定したエフェクトを作成
            GameObject go = (GameObject)Instantiate(Effect01, pos, Quaternion.identity);

            // エフェクトを消す
            Destroy(go, 0.4f);
        }
    }
}
